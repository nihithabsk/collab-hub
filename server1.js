const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const portfinder = require('portfinder');
const path = require('path');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

// Serve static files from the root directory
app.use(express.static(__dirname));

// Serve your HTML file
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index1.html'));
});

io.on('connection', (socket) => {
    console.log('a user connected');
    
    socket.on('chat message', (msg) => {
        io.emit('chat message', msg);
    });

    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
});

// Use portfinder to find an available port and start the server
portfinder.getPort((err, port) => {
    if (err) throw err;
    server.listen(port, () => {
        console.log(`Server is running at http://localhost:${port}`);
    });
});
